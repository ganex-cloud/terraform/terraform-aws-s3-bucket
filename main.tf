resource "aws_s3_bucket" "name" {
  bucket        = "${var.name}"
  force_destroy = "${var.force_destroy}"
  tags          = "${var.tags}"
  policy        = "${var.policy}"
  acl           = "${var.acl}"
  region        = "${var.region}"

  versioning {
    enabled = "${var.versioned}"
  }

  lifecycle_rule = "${var.lifecycle_rule}"
}
