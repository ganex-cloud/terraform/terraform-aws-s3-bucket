module "s3-BUCKET-NAME" {
  source    = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-s3-bucket?ref=master"
  name      = "BUCKET-NAME"
  versioned = "true"

  tags = {
    Name = "Bucket S3"
  }
}
